\babel@toc {spanish}{}
\contentsline {section}{\tocsection {}{1}{Conjuntos}}{2}{section.1}
\contentsline {subsection}{\tocsubsection {}{1.1}{Construcciones básicas}}{2}{subsection.1.1}
\contentsline {subsection}{\tocsubsection {}{1.2}{Aplicaciones}}{11}{subsection.1.2}
\contentsline {subsection}{\tocsubsection {}{1.3}{Conjuntos cociente}}{19}{subsection.1.3}
\contentsline {subsection}{\tocsubsection {}{1.4}{Factorización canónica de una aplicación}}{21}{subsection.1.4}
\contentsline {section}{\tocsection {}{2}{Grupos}}{23}{section.2}
\contentsline {subsection}{\tocsubsection {}{2.1}{Definiciones básicas}}{23}{subsection.2.1}
\contentsline {subsection}{\tocsubsection {}{2.2}{El grupo simétrico}}{27}{subsection.2.2}
\contentsline {subsection}{\tocsubsection {}{2.3}{Ciclos y trasposiciones}}{28}{subsection.2.3}
\contentsline {subsection}{\tocsubsection {}{2.4}{El signo de una permutación}}{34}{subsection.2.4}
\contentsline {subsection}{\tocsubsection {}{2.5}{Subgrupos}}{36}{subsection.2.5}
\contentsline {subsection}{\tocsubsection {}{2.6}{El teorema de Lagrange}}{38}{subsection.2.6}
\contentsline {subsection}{\tocsubsection {}{2.7}{Homomorfismos}}{40}{subsection.2.7}
\contentsline {subsection}{\tocsubsection {}{2.8}{Grupos cociente}}{44}{subsection.2.8}
\contentsline {section}{\tocsection {}{3}{Enteros}}{49}{section.3}
\contentsline {subsection}{\tocsubsection {}{3.1}{Anillos}}{49}{subsection.3.1}
\contentsline {subsection}{\tocsubsection {}{3.2}{Homomorfismos}}{52}{subsection.3.2}
\contentsline {subsection}{\tocsubsection {}{3.3}{Ideales}}{54}{subsection.3.3}
\contentsline {subsection}{\tocsubsection {}{3.4}{Cocientes}}{55}{subsection.3.4}
\contentsline {subsection}{\tocsubsection {}{3.5}{Dominios}}{57}{subsection.3.5}
\contentsline {subsection}{\tocsubsection {}{3.6}{Ideales primos}}{58}{subsection.3.6}
\contentsline {subsection}{\tocsubsection {}{3.7}{Divisibilidad en \(\mathbb {Z}\)}}{59}{subsection.3.7}
\contentsline {subsection}{\tocsubsection {}{3.8}{Divisor común máximo}}{61}{subsection.3.8}
\contentsline {subsection}{\tocsubsection {}{3.9}{Primos}}{66}{subsection.3.9}
\contentsline {subsection}{\tocsubsection {}{3.10}{Congruencias}}{68}{subsection.3.10}
\contentsline {section}{\tocsection {}{4}{Polinomios}}{71}{section.4}
\contentsline {subsection}{\tocsubsection {}{4.1}{Anillos de polinomios}}{71}{subsection.4.1}
\contentsline {subsection}{\tocsubsection {}{4.2}{Irreducibles}}{75}{subsection.4.2}
\contentsline {subsection}{\tocsubsection {}{4.3}{Coeficientes complejos y reales}}{76}{subsection.4.3}
\contentsline {subsection}{\tocsubsection {}{4.4}{Coeficientes enteros y racionales}}{77}{subsection.4.4}
